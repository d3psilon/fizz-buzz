import express = require("express");
const app: express.Application = express();

interface Config {
  api: {
    port: number;
  };
}
interface Keywords {
  [key: string]: number;
}

const config: Config = require("./../config");
let keywords: any = {};

// Allow CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});

// Allow express to use JSON
app.use(express.json());

// saveKeyword allow to save a new world or increment count on it if already exist
function saveKeyword(keyword: string) {
  if (keywords[keyword]) {
    keywords[keyword]++;
  } else {
    keywords[keyword] = 1;
  }
}

// Default GET Request
app.get("/", function(req, res) {
  // int1, int2 and limit must be integer
  if (req.query.int1 && isNaN(req.query.int1)) {
    res.send("int1 must be a number");
    return;
  }
  if (req.query.int2 && isNaN(req.query.int2)) {
    res.send("int2 must be a number");
    return;
  }
  if (req.query.limit && isNaN(req.query.limit)) {
    res.send("limit must be a number");
    return;
  }

  // Define all parameters
  const str1: string = req.query.str1 ? req.query.str1 : "chou";
  const str2: string = req.query.str2 ? req.query.str2 : "croute"; // Désolé mon coté Alsacien ;)
  const str3: string = str1 + str2;
  const int1: number = req.query.int1 ? parseInt(req.query.int1) : 3;
  const int2: number = req.query.int2 ? parseInt(req.query.int2) : 5;
  const int3: number = int1 * int2;
  const limit: number = req.query.limit ? parseInt(req.query.limit) : 100;

  if (req.query.str1) {
    saveKeyword(req.query.str1);
  }

  if (req.query.str2) {
    saveKeyword(req.query.str2);
  }

  // Set result var
  let result: string = "";

  // Loop to generate result
  for (let index = 1; index < limit + 1; index++) {
    let curr: string | number = index;
    if (index % int1 === 0) {
      curr = str1;
    }
    if (index % int2 === 0) {
      curr = str2;
    }
    if (index % int3 === 0) {
      curr = str3;
    }
    result += curr;
    if (index !== limit) {
      result += ",";
    }
  }

  // Send the result
  res.send(result);
});

// Get the list of most used keywords
app.get("/most-used", function(req, res) {
  let sortable: any[] = [];

  // Prepare a sortable array
  for (let keyword in keywords) {
    sortable.push({ keyword: keyword, count: keywords[keyword] });
  }

  // Sort by count value
  sortable.sort(function(a, b) {
    return b.count - a.count;
  });

  // Send the result
  res.json(sortable);
});

// Serve on port
app.listen(config.api.port, function() {
  console.log("Listening on port " + config.api.port);
});
