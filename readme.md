# Fizz-buzz API

## Prerequisite

    Yan must have nodejs server

## Installing dependencies

    npm install

## To build the app

    npm run tsc

## To start server (prod)

    npm run prod

## To start server (develop / watch and compile)

    npm run dev

## To use api

### localhost:7777

    By default it return result like ?str1=chou&str2=croute&int1=3&int2=5&limit=100

    You can add ?str1=x to replace the default value "chou"
    You can add ?str2=x to replace the default value "croute"
    You can add ?int1=x to replace the default value "3"
    You can add ?int2=x to replace the default value "5"
    You can add ?limit=x to replace the default value "100"

### localhost:7777/most-used

    Get all used keywords sorted by count
