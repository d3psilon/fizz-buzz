"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var app = express();
var config = require("./../config");
var keywords = {};
// Allow CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});
// Allow express to use JSON
app.use(express.json());
// saveKeyword allow to save a new world or increment count on it if already exist
function saveKeyword(keyword) {
  if (keywords[keyword]) {
    keywords[keyword]++;
  } else {
    keywords[keyword] = 1;
  }
}
// Default GET Request
app.get("/", function(req, res) {
  // int1, int2 and limit must be integer
  if (req.query.int1 && isNaN(req.query.int1)) {
    res.send("int1 must be a number");
    return;
  }
  if (req.query.int2 && isNaN(req.query.int2)) {
    res.send("int2 must be a number");
    return;
  }
  if (req.query.limit && isNaN(req.query.limit)) {
    res.send("limit must be a number");
    return;
  }
  // Define all parameters
  var str1 = req.query.str1 ? req.query.str1 : "chou";
  var str2 = req.query.str2 ? req.query.str2 : "croute"; // Désolé mon coté Alsacien ;)
  var str3 = str1 + str2;
  var int1 = req.query.int1 ? parseInt(req.query.int1) : 3;
  var int2 = req.query.int2 ? parseInt(req.query.int2) : 5;
  var int3 = int1 * int2;
  var limit = req.query.limit ? parseInt(req.query.limit) : 100;
  if (req.query.str1) {
    saveKeyword(req.query.str1);
  }
  if (req.query.str2) {
    saveKeyword(req.query.str2);
  }
  // Set result var
  var result = "";
  // Loop to generate result
  for (var index = 1; index < limit + 1; index++) {
    var curr = index;
    if (index % int1 === 0) {
      curr = str1;
    }
    if (index % int2 === 0) {
      curr = str2;
    }
    if (index % int3 === 0) {
      curr = str3;
    }
    result += curr;
    if (index !== limit) {
      result += ",";
    }
  }
  // Send the result
  res.send(result);
});
// Get the list of most used keywords
app.get("/most-used", function(req, res) {
  var sortable = [];
  // Prepare a sortable array
  for (var keyword in keywords) {
    sortable.push({ keyword: keyword, count: keywords[keyword] });
  }
  // Sort by count value
  sortable.sort(function(a, b) {
    return b.count - a.count;
  });
  // Send the result
  res.json(sortable);
});
// Serve on port
app.listen(config.api.port, function() {
  console.log("Listening on port " + config.api.port);
});
